# hyper-cwd-wsl

[![hyper](https://img.shields.io/badge/hyper-v3.0.0-green.svg)](https://github.com/zeit/hyper/releases/tag/3.0.0)
[![npm](https://img.shields.io/npm/v/hyper-cwd-wsl.svg)](https://www.npmjs.com/package/hyper-cwd-wsl)
[![npm downloads](https://img.shields.io/npm/dm/hyper-cwd-wsl.svg)](https://www.npmjs.com/package/hyper-cwd-wsl)
[![gitlab build](https://gitlab.com/takahiro652c/hyper-cwd-wsl/badges/master/build.svg)](https://gitlab.com/takahiro652c/hyper-cwd-wsl/commits/master)
[![gitlab coverage](https://gitlab.com/takahiro652c/hyper-cwd-wsl/badges/master/coverage.svg)](https://gitlab.com/takahiro652c/hyper-cwd-wsl/commits/master)

Hyper+WSLで新しいタブを開いた後に、`"cd ${現在のタブのディレクトリ}"`を実行するプラグイン。

つまり、新しいタブを同じcwdで開くことができます。

*Read this in other languages: [Engulish](https://gitlab.com/takahiro652c/hyper-cwd-wsl/blob/master/README.md)*

このプラグインは[hypercwd](https://github.com/hharnisc/hypercwd)より発想を得ました。

![how dose it work](https://gitlab.com/takahiro652c/hyper-cwd-wsl/uploads/e61f6a711d3ec676d43f48a5141e1b8f/gif4.gif)

## Installation

[Hyperの設定ファイル](https://hyper.is/#config-location)の`plugins`に`hyper-cwd-wsl`を追加してください。

```javascript
module.exports = {
  plugins: [
    'hyper-cwd-wsl'
  ],
}
```

## Configuration

[Hyperの設定ファイル](https://hyper.is/#config-location)の`config`に`hyperCwdWsl`を追加してください。

```javascript
module.exports = {
  config: {
    hyperCwdWsl: {
      titleRegExp: '^user@hostname: (.*)$',
      clearAfter: true
    }
  }
}
```

以下のような設定を行うことができます。

| Key             | Description                                                                 | Type      | Default        |
|:----------------|:----------------------------------------------------------------------------|:----------|:---------------|
| `titleRegExp`   | タイトルからcwdを認識するための正規表現。**必ず設定してください**               | string    | `'^.+: (.+)$'` |
| `defaultDirectory` | cwdが認識できなかった時の代替cwd。(hyperを起動した時のcwd)                  | string    | `null`         |
| `replaceRegExp` | タイトルの置換に用いる。`title = title.replace(replaceRegExp, replaceWith);` | string    | `null`         |
| `replaceWith`   | タイトルの置換に用いる。`title = title.replace(replaceRegExp, replaceWith);` | string    | `''`           |
| `clearAfter`    | `cd`コマンド後に`clear`コマンドを実行する。                                   | boolean   | `false`        |
| `validateCwdEveryTime` | タイトルが変わるたびにcwdが正しいかどうか判定する。                      | boolean   | `false`        |
| `debugMode`     | タイトルと認識したcwdをコンソールに出力する。                                  | boolean   | `false`        |

## How to recognize cwd

このプラグインは、タブのタイトルから正規表現(`titleRegExp`)を用いてcwdを認識します。

**そのため、タブのタイトルにcwdが表示されない環境では正常に動作しません。**

**また、必ず適切な`titleRegExp`を設定してください。**

## Replace title

タブのタイトルを正規表現(`replaceRegExp`)を用いて置換することができます。

## Example

タブのタイトルが`"user@hostname: ${cwd}"`の場合

```javascript
module.exports = {
  config: {
    hyperCwdWsl: {
      titleRegExp: '^user@hostname: (.*)$',
      replaceRegExp: '^user@hostname: (/)$|^user@hostname: /?(.+/)*([^/]+)$',
      replaceWith: '$1$3',
      clearAfter: true,
    }
  }
}
```

![example](https://gitlab.com/takahiro652c/hyper-cwd-wsl/uploads/f993c3a5762be6ed7fa64affb4affa38/gif5.gif)

## License
MIT
