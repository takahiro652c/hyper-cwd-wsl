# hyper-cwd-wsl

[![hyper](https://img.shields.io/badge/hyper-v3.0.0-green.svg)](https://github.com/zeit/hyper/releases/tag/3.0.0)
[![npm](https://img.shields.io/npm/v/hyper-cwd-wsl.svg)](https://www.npmjs.com/package/hyper-cwd-wsl)
[![npm downloads](https://img.shields.io/npm/dm/hyper-cwd-wsl.svg)](https://www.npmjs.com/package/hyper-cwd-wsl)
[![gitlab build](https://gitlab.com/takahiro652c/hyper-cwd-wsl/badges/master/build.svg)](https://gitlab.com/takahiro652c/hyper-cwd-wsl/commits/master)
[![gitlab coverage](https://gitlab.com/takahiro652c/hyper-cwd-wsl/badges/master/coverage.svg)](https://gitlab.com/takahiro652c/hyper-cwd-wsl/commits/master)

Execute the `'cd ${current working directory}'` command after opening the new tab in Hyper for WSL(Windows Subsystem for Linux).

*Read this in other languages: [日本語](https://gitlab.com/takahiro652c/hyper-cwd-wsl/blob/master/README.ja.md)*

This is inspired by the [hypercwd](https://github.com/hharnisc/hypercwd).

![how dose it work](https://gitlab.com/takahiro652c/hyper-cwd-wsl/uploads/e61f6a711d3ec676d43f48a5141e1b8f/gif4.gif)

## Installation

Open [Hyper's configuration file](https://hyper.is/#config-location) and add `hyper-cwd-wsl` to the list of `plugins`.

```javascript
module.exports = {
  plugins: [
    'hyper-cwd-wsl'
  ],
}
```

## Configuration

Open [Hyper's configuration file](https://hyper.is/#config-location) and add `hyperCwdWsl` to the object of `config`.

```javascript
module.exports = {
  config: {
    hyperCwdWsl: {
      titleRegExp: '^user@hostname: (.*)$',
      clearAfter: true
    }
  }
}
```

## Options

| Key             | Description                                                                              | Type      | Default        |
|:----------------|:-----------------------------------------------------------------------------------------|:----------|:---------------|
| `titleRegExp`   | Regular expression for recognizing cwd from tab title. **Be sure to set this properly.** | string    | `'^.+: (.+)$'` |
| `defaultDirectory` | Initial directory.                                                                    | string    | `null` |
| `replaceRegExp` | Plugin replaces title like `title = title.replace(replaceRegExp, replaceWith);`          | string    | `null`         |
| `replaceWith`   | Plugin replaces title like `title = title.replace(replaceRegExp, replaceWith);`          | string    | `''`           |
| `clearAfter`    | Execute `clear` command after executing `cd` command.                                    | boolean   | `false`        |
| `validateCwdEveryTime` | Validate cwd every time tab title is changed.                                     | boolean   | `false`        |
| `debugMode`     | Output tab title & cwd to console.                                                       | boolean   | `false`        |

## How to recognize cwd

This plugin recognizes cwd from tab title using regular expression (`titleRegExp`).

**Therefore, this plugin will not operate properly if cwd is not displayed in the title.**

**Also, be sure to set `titleRegExp` properly.**

## Replace title

You can replace tab title by using regular expression (`replaceRegExp`).

## Example

If tab title is `"user@hostname: ${cwd}"`,

```javascript
module.exports = {
  config: {
    hyperCwdWsl: {
      titleRegExp: '^user@hostname: (.*)$',
      replaceRegExp: '^user@hostname: (/)$|^user@hostname: /?(.+/)*([^/]+)$',
      replaceWith: '$1$3',
      clearAfter: true,
    }
  }
}
```

![example](https://gitlab.com/takahiro652c/hyper-cwd-wsl/uploads/f993c3a5762be6ed7fa64affb4affa38/gif5.gif)

## License
MIT
