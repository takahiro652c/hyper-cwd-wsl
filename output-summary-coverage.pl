use strict;
use warnings;

my @coverage_ratios;
my $sum = 0;

while(<>) {
  print;
  if (/^=+ Coverage summary =+$/) {
    last;
  }
}

while(<>) {
  print;
  if (/^\w+\s*: (\d+(\.\d+)?)%/) {
    push(@coverage_ratios, $1);
  }
  if (/^=+$/) {
    foreach my $ratio(@coverage_ratios) {
      $sum += $ratio;
    }
    print('Total coverage: ' . sprintf("%.2f", ($sum / scalar(@coverage_ratios))) . "%\n");
    last;
  }
}

while(<>) {
  print;
}

