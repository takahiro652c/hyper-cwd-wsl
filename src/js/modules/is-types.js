/**
 * strが文字列か判定し、結果を返す。
 *
 * @export
 * @param {*} str
 * @returns {boolean}
 */
export function isString(str) {
  return typeof str === 'string';
}

/**
 * boolがbooleanか判定し、結果を返す。
 *
 * @export
 * @param {*} bool
 * @returns {boolean}
 */
export function isBoolean(bool) {
  return typeof bool === 'boolean';
}

/**
 * objがオブジェクトか判定し、結果を返す。
 *
 * @export
 * @param {*} obj
 * @returns {boolean}
 */
export function isObject(obj) {
  return typeof obj === 'object' && obj !== null;
}
