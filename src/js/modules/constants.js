export const EXECUTE_CD = 'execute hyper cd';
export const DEFAULT_CONFIG = {
  titleRegExp: new RegExp('^.+: (.+)$'),
  defaultDirectory: null,
  replaceRegExp: null,
  replaceWith: '',
  clearAfter: false,
  validateCwdEveryTime: false,
  debugMode: false
};
