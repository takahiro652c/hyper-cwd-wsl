import { isCwdValid } from './is-cwd-valid';

/**
 * 引数cwdをpathと認識できるように変換し、結果を返す。
 * もしcwdが不正ならば空文字列('')を返す。
 *
 * @example
 * result = escapeCwd("/mnt/");
 * // result = "/'mnt'/''"
 * result = escapeCwd("~");
 * // result = "~"
 * result = escapeCwd("~/'foo bar'");
 * // result = `~/''\''foo bar'\'''`
 * result = escapeCwd("~&& echo foo");
 * // result = ""
 * result = escapeCwd("~/&& echo foo");
 * // result = "~/'&& echo foo'"
 *
 * @export
 * @param {string} cwd 変換したいpathの文字列。
 * @returns {string} 変換した結果。
 *
 */
export function escapeCwd(cwd) {
  if (cwd === '~') {
    return '~';
  }
  if (!isCwdValid(cwd)) {
    return '';
  }
  return (
    cwd
      // /mnt/c/'hoge " fuga'/
      .replace(/'/g, "'\\''")
      // -> /mnt/c/'\''hoge " fuga'\''/
      .replace(/\//g, "'/'")
      // -> '/'mnt'/'c'/''\''hoge " fuga'\'''/'
      .replace("'/'", "/'") +
    // -> /'mnt'/'c'/''\''hoge " fuga'\'''/'
    "'"
    // -> /'mnt'/'c'/''\''hoge " fuga'\'''/''
  );
}
