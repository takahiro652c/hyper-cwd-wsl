import { isString } from './is-types';

/**
 * cwdが正しいpathか判定し、結果を返す。
 * 正しいと判定するpathは以下のいずれか。
 * 1.'~'
 * 2.'/' から始まる文字列
 * 3.'~/' から始まる文字列
 *
 * @export
 * @param {string} cwd
 * @returns {boolean}
 */
export function isCwdValid(cwd) {
  return (
    !!cwd &&
    isString(cwd) &&
    (cwd === '~' || cwd[0] === '/' || cwd.substr(0, 2) === '~/')
  );
}
