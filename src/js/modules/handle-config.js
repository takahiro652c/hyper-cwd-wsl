import { DEFAULT_CONFIG } from './constants';
import { isString, isBoolean, isObject } from './is-types';
import { isCwdValid } from './is-cwd-valid';

/**
 * ユーザが設定していればその値を、
 * 設定していなければデフォルトの値を代入したオブジェクトを返す。
 *
 * @export
 * @param {object} userConfig .hyper.jsでユーザが設定したオブジェクト。
 * @returns {object} 各設定を持つオブジェクト。
 */
export function handleConfig(userConfig) {
  const config = Object.assign({}, DEFAULT_CONFIG);
  if (isObject(userConfig)) {
    if (isString(userConfig.titleRegExp)) {
      config.titleRegExp = new RegExp(userConfig.titleRegExp);
    }
    if (isCwdValid(userConfig.defaultDirectory)) {
      config.defaultDirectory = userConfig.defaultDirectory;
    }
    if (!!userConfig.replaceRegExp && isString(userConfig.replaceRegExp)) {
      config.replaceRegExp = new RegExp(userConfig.replaceRegExp);
    }
    if (isString(userConfig.replaceWith)) {
      config.replaceWith = userConfig.replaceWith;
    }
    if (isBoolean(userConfig.clearAfter)) {
      config.clearAfter = userConfig.clearAfter;
    }
    if (isBoolean(userConfig.validateCwdEveryTime)) {
      config.validateCwdEveryTime = userConfig.validateCwdEveryTime;
    }
    if (isBoolean(userConfig.debugMode)) {
      config.debugMode = userConfig.debugMode;
    }
  }
  return config;
}
