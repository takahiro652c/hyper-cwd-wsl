import { EXECUTE_CD } from './constants';
import { handleConfig } from './handle-config';
import { isCwdValid } from './is-cwd-valid';

const cwds = {};
let config = handleConfig();

export default store => next => action => {
  let nextAction = action;
  switch (action.type) {
    case 'CONFIG_LOAD':
    case 'CONFIG_RELOAD': {
      config = handleConfig(action.config.hyperCwdWsl);
      break;
    }
    case 'SESSION_ADD': {
      const activeUid = store.getState().sessions.activeUid;
      let cwd = cwds[activeUid];
      if (!isCwdValid(cwd)) {
        cwd = config.defaultDirectory;
      }
      if (isCwdValid(cwd)) {
        if (config.debugMode) {
          console.log('new tab cwd:');
          console.log(cwd);
        }

        window.rpc.emit(EXECUTE_CD, {
          uid: action.uid,
          cwd,
          clearAfter: config.clearAfter
        });
      }
      break;
    }
    case 'SESSION_SET_XTERM_TITLE': {
      const results = config.titleRegExp.exec(action.title);
      if (config.debugMode) {
        console.log('title:');
        console.log(action.title);
        console.log('cwd:');
        console.log(results ? results[1] : '');
      }
      const cwd = results ? results[1] : false;
      if (!!cwd && (!config.validateCwdEveryTime || isCwdValid(cwd))) {
        cwds[action.uid] = cwd;
      }
      if (config.replaceRegExp) {
        if (config.debugMode) {
          console.log('replaceRegExp:');
          console.log(config.replaceRegExp.exec(action.title));
        }
        let title = action.title.replace(
          config.replaceRegExp,
          config.replaceWith
        );
        nextAction = Object.assign({}, action, { title });
      }
      break;
    }
    case 'SESSION_PTY_EXIT':
    case 'SESSION_USER_EXIT':
      delete cwds[action.uid];
      break;
  }
  next(nextAction);
};
