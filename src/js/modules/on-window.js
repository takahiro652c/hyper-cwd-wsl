import { EXECUTE_CD } from './constants';
import { escapeCwd } from './escape-cwd';

const registeredWindows = [];

export default win => {
  if (registeredWindows.includes(win)) {
    return;
  }
  registeredWindows.push(win);
  win.rpc.on(EXECUTE_CD, obj => {
    let cwd;
    if (!obj || !(cwd = escapeCwd(obj.cwd))) {
      return;
    }
    const session = win.sessions.get(obj.uid);
    if (session) {
      const command = `cd ${cwd} ${obj.clearAfter ? '&& clear' : ''}\n`;
      session.write(command);
    }
  });
};
