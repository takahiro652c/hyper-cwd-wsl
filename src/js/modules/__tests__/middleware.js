import middleware from '../middleware';
import { DEFAULT_CONFIG, EXECUTE_CD } from '../constants';

let rpc;

beforeEach(() => {
  rpc = global.rpc;
});

afterEach(() => {
  global.rpc = rpc;
});

test('test exports.middleware', () => {
  global.rpc = { emit: jest.fn().mockName('window.rpc.emit') };
  const activeUid = 'mock uid';
  const store = {
    getState: jest
      .fn()
      .mockReturnValue({ sessions: { activeUid } })
      .mockName('store.getState')
  };
  const next = jest.fn().mockName('next');
  let action = {};
  let nextCount = 0;
  let emitCount = 0;

  /*
   * test: 呼び出すことができるか。
   */
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds == {};
   * config == {};
   */

  /*
   * debug: debugModeを設定する。
   */
  const debugMode = true;
  action = {
    type: 'CONFIG_LOAD',
    config: { hyperCwdWsl: { debugMode } }
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds == {};
   * config == {
   *   debugMode
   * };
   */

  /*
   * test: cdコマンドの実行をemitしないか。
   */
  action = {
    type: 'SESSION_ADD',
    uid: 'test: cd defaultDirectory'
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  expect(global.rpc.emit.mock.calls.length).toBe(emitCount);
  nextCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds == {};
   * config == {
   *   debugMode
   * };
   */

  /*
   * test: cdコマンドの実行をemitするか。(defaultDirectory)
   */
  // 1: defaultDirectoryを設定する。
  const defaultDirectory = '~';
  action = {
    type: 'CONFIG_RELOAD',
    config: { hyperCwdWsl: { defaultDirectory, debugMode } }
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // final: cd defaultDirectoryが動作するかテストする。
  action = {
    type: 'SESSION_ADD',
    uid: 'test: cd defaultDirectory'
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  expect(global.rpc.emit.mock.calls[emitCount][0]).toBe(EXECUTE_CD);
  expect(global.rpc.emit.mock.calls[emitCount][1]).toEqual({
    uid: action.uid,
    cwd: defaultDirectory,
    clearAfter: DEFAULT_CONFIG.clearAfter
  });
  nextCount++;
  emitCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds == {};
   * config == {
   *   defaultDirectory,
   *   debugMode
   * };
   */

  /*
   * test: cdコマンドの実行をemitするか。(cwd)
   */
  // 1: titleRegExpを設定する。
  const titleRegExp = '^user> (.*)$';
  action = {
    type: 'CONFIG_RELOAD',
    config: { hyperCwdWsl: { titleRegExp, debugMode } }
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // 2: cwdを変更する。
  const cwd = '/foo';
  action = {
    type: 'SESSION_SET_XTERM_TITLE',
    uid: activeUid,
    title: `user> ${cwd}`
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // final: cd cwdが動作するかテストする。
  action = {
    type: 'SESSION_ADD',
    uid: 'test: cd cwd'
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  expect(global.rpc.emit.mock.calls[emitCount][0]).toBe(EXECUTE_CD);
  expect(global.rpc.emit.mock.calls[emitCount][1]).toEqual({
    uid: action.uid,
    cwd,
    clearAfter: DEFAULT_CONFIG.clearAfter
  });
  nextCount++;
  emitCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds[activeUid] === cwd;
   * config == {
   *   titleRegExp,
   *   debugMode
   * };
   */

  /*
   * test: 閉じたタブのcdコマンドの実行をemitしないか。(cwd)
   */
  // 1: タブを閉じる。
  action = {
    type: 'SESSION_PTY_EXIT',
    uid: activeUid
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // final: cd cwdがemitしないことをテストする。
  action = {
    type: 'SESSION_ADD',
    uid: 'test: not execute cd cwd'
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  expect(global.rpc.emit.mock.calls.length).toBe(emitCount);
  nextCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds == {};
   * config == {
   *   titleRegExp,
   *   debugMode
   * };
   */

  /*
   * test: 任意のコードを実行しないか。
   */
  // 1: cwdを変更する。
  const command = ' && echo foo';
  action = {
    type: 'SESSION_SET_XTERM_TITLE',
    uid: activeUid,
    title: `user> ${command}`
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // final: commandを実行しないことをテストする。
  action = {
    type: 'SESSION_ADD',
    uid: 'test: not execute command'
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  expect(global.rpc.emit.mock.calls.length).toBe(emitCount);
  nextCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds == {};
   * config == {
   *   titleRegExp,
   *   debugMode
   * };
   */

  /*
   * test: タイトルの置換。
   */
  // 1: replaceRegExp, replaceWithを設定する。
  const replaceRegExp = '^(.+)> (.*)$';
  const replaceWith = '$1 $2';
  action = {
    type: 'CONFIG_RELOAD',
    config: {
      hyperCwdWsl: { titleRegExp, replaceRegExp, replaceWith, debugMode }
    }
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // final: タイトルを変更し、置換されるかテストする。
  action = {
    type: 'SESSION_SET_XTERM_TITLE',
    uid: activeUid,
    title: `user> ${cwd}`
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toEqual({
    type: action.type,
    uid: action.uid,
    title: `user ${cwd}`
  });
  nextCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds[activeUid] === cwd;
   * config == {
   *   titleRegExp,
   *   replaceRegExp,
   *   replaceWith,
   *   debugMode
   * };
   */

  /*
   * test: validateCwdEveryTime === trueの場合に不正なcwdをcwdsに保存しないか。
   */
  // 1: validateCwdEveryTimeを設定する。
  const validateCwdEveryTime = true;
  action = {
    type: 'CONFIG_RELOAD',
    config: {
      hyperCwdWsl: { titleRegExp, validateCwdEveryTime, debugMode }
    }
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // 2: タイトルにcommandを記述する。
  action = {
    type: 'SESSION_SET_XTERM_TITLE',
    uid: activeUid,
    title: `user> ${command}`
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  nextCount++;
  // final: commandをcwdsに保存していないことをテストする。
  action = {
    type: 'SESSION_ADD',
    uid: 'test: not execute command'
  };
  middleware(store)(next)(action);
  expect(next.mock.calls[nextCount][0]).toBe(action);
  expect(global.rpc.emit.mock.calls[emitCount][0]).toBe(EXECUTE_CD);
  expect(global.rpc.emit.mock.calls[emitCount][1]).toEqual({
    uid: action.uid,
    cwd,
    clearAfter: DEFAULT_CONFIG.clearAfter
  });
  nextCount++;
  emitCount++;
  /*
   * info: current variable values in middleware.js...
   * cwds[activeUid] === cwd;
   * config == {
   *   titleRegExp,
   *   validateCwdEveryTime,
   *   debugMode
   * };
   */
});
