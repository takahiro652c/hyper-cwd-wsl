import { escapeCwd } from '../escape-cwd/';

test('test escapeCwd()', () => {
  expect(escapeCwd(`/`)).toBe(`/''`);
  expect(escapeCwd(`~`)).toBe(`~`);
  expect(escapeCwd(`/foo`)).toBe(`/'foo'`);
  expect(escapeCwd(`~/foo`)).toBe(`~/'foo'`);
  expect(escapeCwd(`/foo/`)).toBe(`/'foo'/''`);
  expect(escapeCwd(`~/foo/`)).toBe(`~/'foo'/''`);
  expect(escapeCwd(`/foo/bar`)).toBe(`/'foo'/'bar'`);
  expect(escapeCwd(`~/foo/bar`)).toBe(`~/'foo'/'bar'`);
  expect(escapeCwd(`/foo/bar`)).toBe(`/'foo'/'bar'`);
  expect(escapeCwd(`~/foo/bar`)).toBe(`~/'foo'/'bar'`);

  expect(escapeCwd(`/foo bar`)).toBe(`/'foo bar'`);
  expect(escapeCwd(`~/foo bar`)).toBe(`~/'foo bar'`);
  expect(escapeCwd(`/ ; echo foo`)).toBe(`/' ; echo foo'`);
  expect(escapeCwd(`~/ ; echo foo`)).toBe(`~/' ; echo foo'`);
  expect(escapeCwd(`/"foo"`)).toBe(`/'"foo"'`);
  expect(escapeCwd(`~/"foo"`)).toBe(`~/'"foo"'`);

  expect(escapeCwd(`/'foo'`)).toBe(`/''\\''foo'\\'''`);
  expect(escapeCwd(`~/'foo'`)).toBe(`~/''\\''foo'\\'''`);

  expect(escapeCwd(` /`)).toBe(``);
  expect(escapeCwd(` ~`)).toBe(``);
  expect(escapeCwd(`~&&echo foo`)).toBe(``);
  expect(escapeCwd(`~ &&echo foo`)).toBe(``);
  expect(escapeCwd(` /&&echo foo`)).toBe(``);
  expect(escapeCwd(1)).toBe(``);
  expect(escapeCwd(true)).toBe(``);
  expect(escapeCwd(null)).toBe(``);
  expect(escapeCwd(undefined)).toBe(``);
  expect(escapeCwd({})).toBe(``);
});
