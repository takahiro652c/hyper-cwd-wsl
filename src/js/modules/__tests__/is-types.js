import { isString, isBoolean, isObject } from '../is-types/';

test('test isString()', () => {
  expect(isString('')).toBe(true);
  expect(isString(1)).toBe(false);
  expect(isString(true)).toBe(false);
  expect(isString(null)).toBe(false);
  expect(isString(undefined)).toBe(false);
  expect(isString({})).toBe(false);
});

test('test isBoolean()', () => {
  expect(isBoolean('')).toBe(false);
  expect(isBoolean(1)).toBe(false);
  expect(isBoolean(true)).toBe(true);
  expect(isBoolean(null)).toBe(false);
  expect(isBoolean(undefined)).toBe(false);
  expect(isBoolean({})).toBe(false);
});

test('test isObject()', () => {
  expect(isObject('')).toBe(false);
  expect(isObject(1)).toBe(false);
  expect(isObject(true)).toBe(false);
  expect(isObject(null)).toBe(false);
  expect(isObject(undefined)).toBe(false);
  expect(isObject({})).toBe(true);
});
