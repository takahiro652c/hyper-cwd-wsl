import { isCwdValid } from '../is-cwd-valid/';

test('test isCwdValid()', () => {
  expect(isCwdValid('~')).toBe(true);
  expect(isCwdValid('/')).toBe(true);
  expect(isCwdValid('/foo')).toBe(true);
  expect(isCwdValid('/foo/')).toBe(true);
  expect(isCwdValid('~/')).toBe(true);
  expect(isCwdValid('~/foo')).toBe(true);
  expect(isCwdValid('~/foo/')).toBe(true);
  expect(isCwdValid('')).toBe(false);
  expect(isCwdValid(' ~')).toBe(false);
  expect(isCwdValid(' ~/')).toBe(false);
  expect(isCwdValid(' /')).toBe(false);
  expect(isCwdValid('foo')).toBe(false);

  expect(isCwdValid(null)).toBe(false);
  expect(isCwdValid(undefined)).toBe(false);
  expect(isCwdValid(1)).toBe(false);
  expect(isCwdValid({})).toBe(false);
});
