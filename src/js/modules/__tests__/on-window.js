import onWindow from '../on-window';
import { EXECUTE_CD } from '../constants';
import { escapeCwd } from '../escape-cwd';

test('test exports.onWindow', () => {
  const writeFn = jest.fn().mockName('window.session.get.write');
  const win = {
    rpc: {
      on: jest.fn().mockName('window.rpc.on')
    },
    sessions: {
      get: jest
        .fn()
        .mockReturnValueOnce(null)
        .mockReturnValue({ write: writeFn })
        .mockName('window.session.get')
    }
  };
  let winSessionGetCount = 0;
  let writeCount = 0;

  onWindow(win);
  expect(win.rpc.on.mock.calls[0][0]).toBe(EXECUTE_CD);
  const executeCdFn = win.rpc.on.mock.calls[0][1];
  const cwd = '/mock cwd';
  const escapedCwd = escapeCwd(cwd);
  const uid = 'mock uid';
  // 最初はwin.sessions.get()がnullを返すのでwrite()が呼ばれない
  executeCdFn({ cwd, uid, clearAfter: true });
  expect(win.sessions.get.mock.calls[winSessionGetCount][0]).toBe(uid);
  expect(writeFn.mock.calls.length).toBe(writeCount);
  winSessionGetCount++;

  // 次はwin.sessions.get()が{write}を返すのでwrite()が呼ばれる
  executeCdFn({ cwd, uid, clearAfter: true });
  expect(win.sessions.get.mock.calls[winSessionGetCount][0]).toBe(uid);
  expect(writeFn.mock.calls[writeCount][0]).toBe(`cd ${escapedCwd} && clear\n`);
  winSessionGetCount++;
  writeCount++;

  // clearAfter: falseの場合
  executeCdFn({ cwd, uid, clearAfter: false });
  expect(win.sessions.get.mock.calls[winSessionGetCount][0]).toBe(uid);
  expect(writeFn.mock.calls[writeCount][0]).toBe(`cd ${escapedCwd} \n`);
  winSessionGetCount++;
  writeCount++;

  // cwdが不正な場合
  const invalidCwd = 'invalid';
  executeCdFn({ invalidCwd, uid, clearAfter: true });
  expect(win.sessions.get.mock.calls.length).toBe(winSessionGetCount);
  expect(writeFn.mock.calls.length).toBe(writeCount);

  const invalidCwd2 = '';
  executeCdFn({ invalidCwd2, uid, clearAfter: true });
  expect(win.sessions.get.mock.calls.length).toBe(winSessionGetCount);
  expect(writeFn.mock.calls.length).toBe(writeCount);
});
