import { handleConfig } from '../handle-config/';
import { DEFAULT_CONFIG } from '../constants';

test('test handleConfig()', () => {
  let config = {};
  let result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: '',
    defaultDirectory: '',
    replaceRegExp: '',
    replaceWith: '',
    clearAfter: '',
    validateCwdEveryTime: '',
    debugMode: ''
  };
  result = Object.assign({}, DEFAULT_CONFIG, {
    titleRegExp: new RegExp(''),
    replaceWith: ''
  });
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: 'a',
    defaultDirectory: 'a',
    replaceRegExp: 'a',
    replaceWith: 'a',
    clearAfter: 'a',
    validateCwdEveryTime: 'a',
    debugMode: 'a'
  };
  result = Object.assign({}, DEFAULT_CONFIG, {
    titleRegExp: /a/,
    replaceRegExp: /a/,
    replaceWith: 'a'
  });
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: '/a',
    defaultDirectory: '/a',
    replaceRegExp: '/a',
    replaceWith: '/a',
    clearAfter: '/a',
    validateCwdEveryTime: '/a',
    debugMode: '/a'
  };
  result = Object.assign({}, DEFAULT_CONFIG, {
    titleRegExp: /\/a/,
    defaultDirectory: '/a',
    replaceRegExp: /\/a/,
    replaceWith: '/a'
  });
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: 1,
    defaultDirectory: 1,
    replaceRegExp: 1,
    replaceWith: 1,
    clearAfter: 1,
    validateCwdEveryTime: 1,
    debugMode: 1
  };
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: true,
    defaultDirectory: true,
    replaceRegExp: true,
    replaceWith: true,
    clearAfter: true,
    validateCwdEveryTime: true,
    debugMode: true
  };
  result = Object.assign({}, DEFAULT_CONFIG, {
    clearAfter: true,
    validateCwdEveryTime: true,
    debugMode: true
  });
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: false,
    defaultDirectory: false,
    replaceRegExp: false,
    replaceWith: false,
    clearAfter: false,
    validateCwdEveryTime: false,
    debugMode: false
  };
  result = Object.assign({}, DEFAULT_CONFIG, {
    clearAfter: false,
    validateCwdEveryTime: false,
    debugMode: false
  });
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: null,
    defaultDirectory: null,
    replaceRegExp: null,
    replaceWith: null,
    clearAfter: null,
    validateCwdEveryTime: null,
    debugMode: null
  };
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: undefined,
    defaultDirectory: undefined,
    replaceRegExp: undefined,
    replaceWith: undefined,
    clearAfter: undefined,
    validateCwdEveryTime: undefined,
    debugMode: undefined
  };
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: {},
    defaultDirectory: {},
    replaceRegExp: {},
    replaceWith: {},
    clearAfter: {},
    validateCwdEveryTime: {},
    debugMode: {}
  };
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = {
    titleRegExp: /a/,
    defaultDirectory: /a/,
    replaceRegExp: /a/,
    replaceWith: /a/,
    clearAfter: /a/,
    validateCwdEveryTime: /a/,
    debugMode: /a/
  };
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = 1;
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = '';
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = true;
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = null;
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);

  config = undefined;
  result = Object.assign({}, DEFAULT_CONFIG, {});
  expect(handleConfig(config)).toEqual(result);
});
