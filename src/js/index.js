import hyperCwdWslOnWindow from './modules/on-window';
import hyperCwdWslMiddleware from './modules/middleware';

export const onWindow = hyperCwdWslOnWindow;
export const middleware = hyperCwdWslMiddleware;
